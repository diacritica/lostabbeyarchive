---
title: "City 12"
date: 2020-12-28T14:46:10+06:00
description: "Medieval city sketch"
type: "post"
image: "images/city/city-12.jpg"
categories: 
  - "Cities"
tags:
  - "Cities"
---

> [Download image](../images/city/city-12.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




