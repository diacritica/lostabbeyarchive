---
title: "Dress 03"
date: 2020-10-01T14:46:10+06:00
description: "Medieval dress"
type: "post"
image: "images/clothing/dress-03.jpg"
categories: 
  - "Clothing"
tags:
  - "Clothes"
  - "Dress"
---

> [Download image](../images/clothing/dress-03.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




