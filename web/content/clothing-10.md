---
title: "Jester"
date: 2020-10-04T14:46:10+06:00
description: "Medieval Jester outfit"
type: "post"
image: "images/clothing/jester.jpg"
categories: 
  - "Clothing"
tags:
  - "Clothes"
  - "Hats"
---

> [Download image](../images/clothing/jester.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




