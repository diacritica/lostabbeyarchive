---
title: "Torch 01"
date: 2020-10-22T14:46:10+06:00
description: "Medieval Torch"
type: "post"
image: "images/objects/torch-01.jpg"
categories: 
  - "Objects"
tags:
---

> [Download image](../images/objects/torch-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




