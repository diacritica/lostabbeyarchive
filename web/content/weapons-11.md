---
title: "Axes"
date: 2020-12-28T14:46:10+06:00
description: "Medieval axes"
type: "post"
image: "images/weapons/axes.jpg"
categories: 
  - "Weapons"
tags:

---

> [Download image](../images/weapons/axes.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




