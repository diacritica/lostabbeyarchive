---
title: "Boots 01"
date: 2020-10-04T14:46:10+06:00
description: "Medieval leather boots"
type: "post"
image: "images/clothing/boots-01.jpg"
categories: 
  - "Clothing"
tags:
  - "Clothes"
  - "Hats"
---

> [Download image](../images/clothing/boots-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




