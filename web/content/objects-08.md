---
title: "Crown 01"
date: 2020-03-01T14:46:10+06:00
description: "Medieval crown sketch"
type: "post"
image: "images/objects/crown-01.jpg"
categories: 
  - "Objects"
tags:
  - "Crown"
---

> [Download image](../images/objects/crown-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




