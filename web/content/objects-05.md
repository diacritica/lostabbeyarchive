---
title: "Scroll 01"
date: 2020-02-11T14:46:10+06:00
description: "Old Scroll sketch"
type: "post"
image: "images/objects/scroll-01.jpg"
categories: 
  - "Objects"
tags:
  - "Scroll"
---

> [Download image](../images/objects/scroll-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




