---
title: "About"
date: 2019-05-14T14:46:10+06:00
description: "About the Lost Abbey Archive"
image: "images/author.jpg"
---
I am passionate about antique European architecture, art and lifestyle. It has always caught my attention. In particular, I have treasured the culture, science and evolution of European civilization in medieval times.
This large compilation of quick sketches (5 - 15 min) represents my personal tribute to this period of our history.

The primary inspiration comes from the Late Middle Ages, respecting its plausability but adding here and there some idealization touches as part of my creative freedom.

### Would you like to use any of these sketches?

Besides the free exploration of the entire catalogue, you might want to use some of the illustrations for fun or a project. You are very welcome to do so!

There is only one simple condition, to honour the [Creative Commons Attribution License](https://creativecommons.org/licenses/by/4.0/) every time you make use of one of the drawings. Credit me "(c) CCBY Juan de la Cruz https://lostabbeyarchive.com".

If you are a writer, feel free to use these pictures to represent your writing, I'd love to see them a great artistic companion to your stories.

If you are an artist, it will be an honor if you use these sketches as the basis of your creations, feel free to modify them as your creativity dictates.

If you are looking for visual references of old medieval times, hopefully you will find here what you are looking for.

If you came to this place unexpectedly and are enjoying its content, that is my greatest achievement.

Whatever your purpose is, this is all for you.

-----------------------------------

- The Lost Abbey Archive by [@elhombretecla](https://twitter.com/elhombretecla)
- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)
