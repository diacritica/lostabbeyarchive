---
title: "Rings 01"
date: 2020-10-21T14:46:10+06:00
description: "Medieval rings"
type: "post"
image: "images/objects/rings-01.jpg"
categories: 
  - "Objects"
tags:
  - "Jewellery"
---

> [Download image](../images/objects/rings-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




