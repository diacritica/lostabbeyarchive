---
title: "Book 02"
date: 2020-02-10T14:46:10+06:00
description: "Old Book sketch"
type: "post"
image: "images/objects/book-02.jpg"
categories: 
  - "Objects"
tags:
  - "Books"
  - "Glass"
  - "Candle"
---

> [Download image](../images/objects/book-02.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




