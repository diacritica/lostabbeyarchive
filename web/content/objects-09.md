---
title: "Chest 01"
date: 2020-03-17T14:46:10+06:00
description: "Medieval chest"
type: "post"
image: "images/objects/chest-01.jpg"
categories: 
  - "Objects"
tags:
  - "Chest"
---

> [Download image](../images/objects/chest-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




