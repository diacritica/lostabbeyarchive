---
title: "City 04"
date: 2020-02-20T14:46:10+06:00
description: "Medieval city sketch"
type: "post"
image: "images/city/city-04.jpg"
categories: 
  - "Cities"
tags:
  - "Cities"
---

> [Download image](../images/city/city-04.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




