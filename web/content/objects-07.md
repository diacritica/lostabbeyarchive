---
title: "Book 05"
date: 2020-03-15T14:46:10+06:00
description: "Old Book sketch"
type: "post"
image: "images/objects/book-05.jpg"
categories: 
  - "Objects"
tags:
  - "Books"
  - "Scroll"
  - "Ink"
---

> [Download image](../images/objects/book-05.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




