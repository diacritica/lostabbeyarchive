---
title: "Crossbow-01"
date: 2020-03-17T14:46:10+06:00
description: "Medieval Crossbow"
type: "post"
image: "images/weapons/crossbow-01.jpg"
categories: 
  - "Weapons"
tags:
  - "crossbow"
---

> [Download image](../images/weapons/crossbow-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




