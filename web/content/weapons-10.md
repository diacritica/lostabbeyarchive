---
title: "Catapult-01"
date: 2020-11-06T14:46:10+06:00
description: "Medieval catapult"
type: "post"
image: "images/weapons/catapult-01.jpg"
categories: 
  - "Weapons"
tags:

---

> [Download image](../images/weapons/catapult-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




