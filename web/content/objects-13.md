---
title: "Mirror 01"
date: 2020-09-26T14:46:10+06:00
description: "Medieval mirror frame"
type: "post"
image: "images/objects/mirror-01.jpg"
categories: 
  - "Objects"
tags:
  - "Mirror"
---

> [Download image](../images/objects/mirror-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




