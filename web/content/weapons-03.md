---
title: "Daggers"
date: 2020-02-25T14:46:10+06:00
description: "Medieval daggers"
type: "post"
image: "images/weapons/daggers-01.jpg"
categories: 
  - "Weapons"
tags:
  - "daggers"
---

> [Download image](../images/weapons/daggers-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




