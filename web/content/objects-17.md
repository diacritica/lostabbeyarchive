---
title: "Arrow target 01"
date: 2020-10-21T14:46:10+06:00
description: "Medieval Arrow target"
type: "post"
image: "images/objects/target-01.jpg"
categories: 
  - "Objects"
tags:
  - "Arrows"
---

> [Download image](../images/objects/target-01.jpg)

- Free for commercial and personal use under [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)




